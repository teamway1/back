<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personality_answers', function (Blueprint $table) {
            $table->id();
            $table->string('answer');
            $table->tinyInteger('value');
            $table->enum('value_type', ['introvert', 'extrovert']);
            $table->foreignId('personality_question_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personality_answers');
    }
};
