<?php

namespace Modules\PersonalityTest\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\PersonalityTest\Entities\PersonalityAnswer;
use Modules\PersonalityTest\Entities\PersonalityQuestion;
use Modules\PersonalityTest\Enums\PersonalityType;

class PersonalityTestDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $questions = PersonalityQuestion::factory()
                ->count(7)
                ->create();

            foreach($questions as $question) {
                PersonalityAnswer::factory([
                    'value_type' => PersonalityType::extrovert,
                    'value' => 1,
                    'personality_question_id' => $question->id
                ])
                    ->count(2)
                    ->create();

                PersonalityAnswer::factory([
                    'value_type' => PersonalityType::introvert,
                    'value' => 1,
                    'personality_question_id' => $question->id
                ])
                    ->count(2)
                    ->create();
            }

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
