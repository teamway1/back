<?php

namespace Modules\PersonalityTest\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\PersonalityTest\Entities\PersonalityAnswer;
use Modules\PersonalityTest\Enums\PersonalityType;

class PersonalityAnswerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PersonalityAnswer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'answer' => $this->faker->text(50),
            'value_type' => $this->faker->randomElement([
                PersonalityType::introvert, PersonalityType::extrovert
            ]),
            'value' => $this->faker->numberBetween(-10, 10)
        ];
    }
}

