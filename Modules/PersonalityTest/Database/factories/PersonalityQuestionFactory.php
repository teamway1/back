<?php

namespace Modules\PersonalityTest\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\PersonalityTest\Entities\PersonalityQuestion;

class PersonalityQuestionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PersonalityQuestion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'question' => $this->faker->text(100),
        ];
    }
}

