<?php

namespace Modules\PersonalityTest\Tests\Unit;

use Modules\PersonalityTest\Enums\PersonalityType;
use Modules\PersonalityTest\Services\PersonalityService;
use Tests\TestCase;

class PersonalityServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_personality_test_resutl()
    {
        $introvert = PersonalityService::calculatePersonalityTest(10, 0);
        $this->assertEquals(PersonalityType::introvert->value, $introvert);

        $extravert = PersonalityService::calculatePersonalityTest(0, 10);
        $this->assertEquals(PersonalityType::extrovert->value, $extravert);

        $breathtaking = PersonalityService::calculatePersonalityTest(10, 10);
        $this->assertEquals(PersonalityService::EQUAL_SCORE, $breathtaking);
    }
}
