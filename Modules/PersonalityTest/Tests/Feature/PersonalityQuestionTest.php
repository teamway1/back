<?php

namespace Modules\PersonalityTest\Tests\Feature;

use Illuminate\Support\Facades\DB;
use Modules\PersonalityTest\Entities\PersonalityAnswer;
use Modules\PersonalityTest\Entities\PersonalityQuestion;
use Modules\PersonalityTest\Enums\PersonalityType;
use Tests\TestCase;
use Throwable;

class PersonalityQuestionTest extends TestCase
{
    /**
     * @throws Throwable
     */
    public function test_list_questions()
    {
        $questionsCount = 3;
        DB::beginTransaction();
        try {
            PersonalityQuestion::factory()
                ->count($questionsCount)->create();
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        $response = $this->get(route('list-personality-questions'));

        $response
            ->assertStatus(200)
            ->assertJsonCount($questionsCount)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'question'
                ]
            ]);
    }

    /**
     * @throws Throwable
     */
    public function test_get_question(): void
    {
        DB::beginTransaction();
        try {
            $question = PersonalityQuestion::factory()->create();

            PersonalityAnswer::factory([
                'personality_question_id' => $question->id
            ])
                ->count(4)
                ->create();
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        $response = $this->get(route('get-personality-questions', ['id' => $question->id]));
        $response->assertOk();
        $response->assertJsonStructure(
            [
                'id',
                'question',
                'answers' => [
                    '*' => ['id', 'answer']
                ]
            ]
        );
    }

    /**
     * @return void
     */
    public function test_store_question(): void
    {
        $testQuestions = 'Test answer 1';
        $extravertAnswer = [
            'answer' => 'I am extravert',
            'value_type' => PersonalityType::extrovert,
            'value' => -1
        ];

        $introvertAnswer = [
            'answer' => 'I am introvert',
            'value_type' => PersonalityType::introvert,
            'value' => 1
        ];

        $toSave = [
            'question' => $testQuestions,
            'answers' => [$extravertAnswer, $introvertAnswer]
        ];

        $request = $this->postJson(route('store-personality-questions'), $toSave);
        $request->assertOk();

        $this->assertDatabaseHas('personality_questions', [
            'question' => $testQuestions
        ]);
        $this->assertDatabaseHas('personality_answers', $extravertAnswer);
        $this->assertDatabaseHas('personality_answers', $introvertAnswer);

        $newIntrovertAnswer = [
            'answer' => 'New introvert answer',
            'value_type' => PersonalityType::introvert,
            'value' => 2
        ];

        $toSave = [
            'id' => $request->json(),
            'question' => $testQuestions,
            'answers' => [$extravertAnswer, $newIntrovertAnswer]
        ];

        $this->postJson(route('store-personality-questions'), $toSave)
            ->assertOk();

        $this->assertDatabaseHas('personality_answers', $extravertAnswer);
        $this->assertDatabaseHas('personality_answers', $newIntrovertAnswer);
        $this->assertSoftDeleted('personality_answers', $introvertAnswer);
    }


    /**
     * @return void
     */
    public function test_store_question_validation(): void
    {
        $testQuestions = 'Test answer 1';

        $request = $this->postJson(route('store-personality-questions'), []);
        $request->assertJsonValidationErrors(['answers', 'question']);

        $toSave = [
            'question' => $testQuestions,
            'answers' => [
                [
                    'answer' => 'I am extravert',
                    'value_type' => PersonalityType::extrovert,
                    'value' => -1
                ]
            ]
        ];
        $request = $this->postJson(route('store-personality-questions'), $toSave);
        $request->assertJsonValidationErrors(['answers']);
    }

    /**
     * @return void
     * @throws Throwable
     */
    public function test_delete_question_validation(): void
    {
        DB::beginTransaction();
        try {
            $question = PersonalityQuestion::factory()->create();

            PersonalityAnswer::factory([
                'personality_question_id' => $question->id
            ])
                ->count(2)
                ->create();
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        $this->assertDatabaseHas('personality_questions', ['id' => $question->id]);

        $request = $this->deleteJson(
            route('delete-personality-questions', ['question' => $question->id]),
        );

        $request->assertOk();
    }
}
