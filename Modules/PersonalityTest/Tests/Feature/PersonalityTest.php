<?php

namespace Modules\PersonalityTest\Tests\Feature;

use Illuminate\Support\Facades\DB;
use Modules\PersonalityTest\Entities\PersonalityAnswer;
use Modules\PersonalityTest\Entities\PersonalityQuestion;
use Modules\PersonalityTest\Enums\PersonalityType;
use Modules\PersonalityTest\Services\PersonalityService;
use Tests\TestCase;
use Throwable;

class PersonalityTest extends TestCase
{
    /**
     * @throws Throwable
     */
    public function test_get_personality()
    {
        $questionsCount = 3;
        DB::beginTransaction();
        try {
            $questions = PersonalityQuestion::factory()->count($questionsCount)->create();
            foreach ($questions as $question) {
                PersonalityAnswer::factory([
                    'personality_question_id' => $question->id
                ])
                    ->count(4)
                    ->create();
            }
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        $response = $this->get(route('get-personality-test'));

        $response
            ->assertStatus(200)
            ->assertJsonCount($questionsCount)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'question',
                    'answers' => [
                        '*' => ['id', 'answer']
                    ]
                ]
            ]);
    }

    /**
     * @throws Throwable
     */
    public function test_personality_test_score() {
        DB::beginTransaction();
        $introvertAnswers = [];
        $extrovertAnswers = [];

        try {
            $questions = PersonalityQuestion::factory()->count(2)->create();
            foreach ($questions as $question) {
                $introvertAnswer = PersonalityAnswer::factory([
                    'personality_question_id' => $question->id,
                    'value_type' => PersonalityType::introvert,
                    'value' => 1,
                ])->create();
                $extrovertAnswer = PersonalityAnswer::factory([
                    'personality_question_id' => $question->id,
                    'value_type' => PersonalityType::extrovert,
                    'value' => 1,
                ])->create();
                $extrovertAnswers[] = $extrovertAnswer->id;
                $introvertAnswers[] = $introvertAnswer->id;
            }
            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        $request = $this->postJson(route('personality-test-score'), ['answers' => $extrovertAnswers]);
        $request->assertOk();
        $request->assertSee(PersonalityType::extrovert->value);

        $request = $this->postJson(route('personality-test-score'), ['answers' => $introvertAnswers]);
        $request->assertOk();
        $request->assertSee(PersonalityType::introvert->value);

        $request = $this->postJson(route('personality-test-score'), [
            'answers' => [$introvertAnswers[0], $extrovertAnswers[1]]
        ]);
        $request->assertOk();
        $request->assertSee(PersonalityService::EQUAL_SCORE);
    }

}
