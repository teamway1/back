<?php

namespace Modules\PersonalityTest\Enums;

enum PersonalityType: string
{
    case introvert = 'introvert';
    case extrovert = 'extrovert';
}
