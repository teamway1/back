<?php

namespace Modules\PersonalityTest\Services;

use Modules\PersonalityTest\Enums\PersonalityType;

class PersonalityService
{
    // Only the Sith make everything absolute
    public const EQUAL_SCORE = 'breathtaking';

    /**
     * @param int $introvertScore
     * @param int $extrovertScore
     * @return string
     */
    public static function calculatePersonalityTest(
        int $introvertScore,
        int $extrovertScore,
    ): string
    {
        if ($extrovertScore > $introvertScore) {
            return PersonalityType::extrovert->value;
        }
        if ($introvertScore > $extrovertScore) {
            return PersonalityType::introvert->value;
        }
        return self::EQUAL_SCORE;
    }
}
