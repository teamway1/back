<?php

use Modules\PersonalityTest\Http\Controllers\PersonalityTestController;
use Modules\PersonalityTest\Http\Controllers\PersonalityTestQuestionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1/personality-test')->group(function () {
    Route::get('/', [PersonalityTestController::class, 'index'])
        ->name('get-personality-test');

    Route::post('/', [PersonalityTestController::class, 'score'])
        ->name('personality-test-score');

});

Route::prefix('/v1/personality-questions')->group(function () {
    Route::get('/', [PersonalityTestQuestionController::class, 'index'])
        ->name('list-personality-questions');

    Route::get('/{id}', [PersonalityTestQuestionController::class, 'get'])
        ->name('get-personality-questions');

    Route::post('/', [PersonalityTestQuestionController::class, 'store'])
        ->name('store-personality-questions');

    Route::delete('/{question}', [PersonalityTestQuestionController::class, 'delete'])
        ->name('delete-personality-questions');
});

