<?php

namespace Modules\PersonalityTest\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Modules\PersonalityTest\Database\factories\PersonalityQuestionFactory;
use Throwable;

class PersonalityQuestion extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = ['id'];

    protected static function newFactory()
    {
        return PersonalityQuestionFactory::new();
    }

    /**
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(PersonalityAnswer::class);
    }

    /**
     * @param Builder $query
     * @param array $fields
     * @return void
     */
    public function scopeWithAnswers(Builder $query, array $fields): void
    {
        $query
            ->with('answers', function($query) use ($fields) {
                $query->select($fields);
            });
    }

    /**
     * @param array $data
     * @return int
     * @throws Throwable
     */
    public static function store(array $data): int
    {
        DB::beginTransaction();
        try {
            /** @var self $question */
            $question = PersonalityQuestion::query()
                ->updateOrCreate(['id' => $data['id'] ?? null], $data);

            $question->syncAnswers($data['answers']);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        return $question->id;
    }

    /**
     * @param array $answers
     * @return void
     */
    public function syncAnswers(array $answers): void
    {
        $this->answers()->delete();
        $toSync = array_map(fn($item) => new PersonalityAnswer($item), $answers);
        $this->answers()->saveMany($toSync);
    }
}
