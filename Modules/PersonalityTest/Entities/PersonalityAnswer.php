<?php

namespace Modules\PersonalityTest\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\PersonalityTest\Database\factories\PersonalityAnswerFactory;

class PersonalityAnswer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['personality_question_id'];
    protected static function newFactory()
    {
        return PersonalityAnswerFactory::new();
    }
}
