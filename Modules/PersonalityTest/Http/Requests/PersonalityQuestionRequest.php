<?php

namespace Modules\PersonalityTest\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;
use Modules\PersonalityTest\Enums\PersonalityType;

class PersonalityQuestionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['sometimes', 'integer'],
            'question' => ['required', 'string', 'max: 255'],
            'answers' => ['required', 'array', 'min:2'],
            'answers.*.id' => ['sometimes', 'integer'],
            'answers.*.answer' => ['required', 'string', 'max: 255'],
            'answers.*.value_type' => [
                'required',
                new Enum(PersonalityType::class)
            ],
            'answers.*.value' => ['required', 'integer'],
        ];
    }
}
