<?php

namespace Modules\PersonalityTest\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalityTestScoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answers' => ['required', 'array'],
            'answers.*' => ['required', 'integer']
        ];
    }
}
