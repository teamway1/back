<?php

namespace Modules\PersonalityTest\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Routing\Controller;
use Modules\PersonalityTest\Entities\PersonalityQuestion;
use Modules\PersonalityTest\Http\Requests\PersonalityQuestionRequest;
use Throwable;

class PersonalityTestQuestionController extends Controller
{
    /**
     * @return Collection
     */
    public function index(): Collection
    {
        return PersonalityQuestion::query()
            ->orderBy('created_at')
            ->get(['id', 'question'])
            ->makeVisible(['id']);
    }

    /**
     * @param $id
     * @return PersonalityQuestion
     */
    public function get($id): PersonalityQuestion
    {
        return PersonalityQuestion::query()
            ->select([
                'id',
                'question',
            ])
            ->withAnswers(['id','answer','personality_question_id', 'value', 'value_type'])
            ->findOrFail($id)
            ->makeVisible(['id']);
    }

    /**
     * @param PersonalityQuestionRequest $request
     * @return int
     * @throws Throwable
     */
    public function store(PersonalityQuestionRequest $request): int {
        return PersonalityQuestion::store($request->validated());
    }

    /**
     * @param PersonalityQuestion $question
     * @return void
     */
    public function delete(PersonalityQuestion $question): void
    {
        $question->delete();
    }
}
