<?php

namespace Modules\PersonalityTest\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Routing\Controller;
use Modules\PersonalityTest\Entities\PersonalityAnswer;
use Modules\PersonalityTest\Entities\PersonalityQuestion;
use Modules\PersonalityTest\Enums\PersonalityType;
use Modules\PersonalityTest\Http\Requests\PersonalityTestScoreRequest;
use Modules\PersonalityTest\Services\PersonalityService;

class PersonalityTestController extends Controller
{
    /**
     * @return Collection
     */
    public function index(): Collection
    {
        return PersonalityQuestion::query()
            ->select(['id', 'question'])
            ->withAnswers(['id','answer','personality_question_id'])
            ->orderBy('created_at')
            ->get();
    }

    /**
     *
     * @param PersonalityTestScoreRequest $request
     * @return string
     */
    public function score(PersonalityTestScoreRequest $request): string {
        $data = $request->validated();
        $answersIds = $data['answers'];

        $introvertScore = PersonalityAnswer::query()
            ->where('value_type', PersonalityType::introvert)
            ->whereIn('id', $answersIds)
            ->sum('value');

        $extrovertScore = PersonalityAnswer::query()
            ->where('value_type', PersonalityType::extrovert)
            ->whereIn('id', $answersIds)
            ->sum('value');

        return PersonalityService::calculatePersonalityTest($introvertScore, $extrovertScore);
    }
}
