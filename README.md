### For local dev:
0) Make sure you have php 8.1 installed with extensions:
```
php8.1-fpm php8.1-cli php8.1-curl php8.1-zip php8.1-mysql php8.1-mbstring php8.1-xml php8.1-bcmath
 ```
1) Make sure you have installed[composer](https://getcomposer.org/) 
2) Copy environment file
```shell
cp .env.example .env
```
3) Make sqlite DB file
```shell
touch database/database.sqlite
```
4) Install composer dependencies
```shell
composer install
```
5) Generate app key
```shell
php artisan key:generate
```
6) Migrate
```shell
php artisan migrate
```
7) Use local php server
```shell
php artisn serve
```

### For tests:
```
php artisan test
```

### For mock:
```
php artisan module:seed
```

#### Modules [docks](https://nwidart.com/laravel-modules/v6/introduction) 
